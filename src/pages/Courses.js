
import coursesData from'../data/coursesData';
import Course from '../components/Course';


export default function Courses(){
	const courses = coursesData.map(course =>{
		return(
			<Course courseProp={course} key={course.id}/>)
	})
	return(
		<>
		
			<h1>Courses</h1>
			{courses}
		
		</>
	)
}