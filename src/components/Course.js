import React, { useState } from 'react';
import {Row, Col, Button, Card} from 'react-bootstrap';

export default function Course({courseProp}){
	console.log(courseProp);

	const{name,description,price} = courseProp;
	const [count, setCount] = useState(0);
	const [seats, setSeats] = useState(30);

	function enroll(){
		setCount(count + 1);
		setSeats(seats - 1);
		if (seats <= 0 ){
			alert("No more seats Available!")
			setCount(0);
			setSeats(30);
		}
	}
	console.log(seats);
	// function seat(){
	// 	setSeats(seats - 1);
	// 	if (seats <)
	// }
	return(<Row className="my-3">
			<Col xs={12} md={4} xl={12}>
				<Card className="cardCourse p-3">
				    <Card.Body>
				        <Card.Title><h4>{name}</h4></Card.Title>
				        <Card.Text><h5>Description</h5>{description}</Card.Text>
				        <Card.Text> Price
				          <Card.Text>{price}</Card.Text>
				        </Card.Text>
				        <Card.Text className="mt-0">Enrollees: {count}</Card.Text>
				          <Card.Text className="mt-0">Seats Available: {seats}</Card.Text>
				        <Button onClick={enroll}>Enroll</Button>
				    </Card.Body>
				</Card>
				</Col>
				</Row>)}